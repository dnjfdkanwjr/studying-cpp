#pragma once
#include <algorithm>
#include <random>
#include <iostream>
#include <iterator>
#include <chrono>

std::default_random_engine dre;

std::uniform_int_distribution<int> nameRandom{ 'a','z' };
std::uniform_int_distribution<int> lenRandom{ 3,10 };

// move test.
// versus copy


class MovingMonster {
	char* name{ nullptr };

	int len{ };
public:
	MovingMonster()noexcept {
		this->len = lenRandom(dre);
		this->name = new char[len];
		for (int i = 0; i < len; ++i)
			this->name[i] = nameRandom(dre);
	}

	MovingMonster(const MovingMonster& other)noexcept {
		this->len = other.len;
		this->name = new char[len];

		for (int i = 0; i < len; ++i) {
			this->name[i] = other.name[i];
		}
	}


	MovingMonster(MovingMonster&& xVal)noexcept {
		this->name = std::move(xVal.name);
		xVal.name = nullptr;
		this->len = xVal.len;
	}
	~MovingMonster()  noexcept {
		delete this->name;
	}

	MovingMonster& operator=(const MovingMonster& other) noexcept {
		this->len = other.len;

		if (this->name != nullptr) delete[] this->name;
		this->name = new char[len];

		for (int i = 0; i < len; ++i) {
			this->name[i] = other.name[i];
		}

		return*this;
	}

	MovingMonster& operator=(MovingMonster&& other) noexcept {
		this->len = other.len;
		if (this->name != nullptr) delete[] this->name;
		this->name = std::move(other.name);
		other.name = nullptr;

		return*this;
	}

	int length() const noexcept {
		return this->len;
	}

	void show(void) const noexcept {
		for (int i = 0; i < len; ++i) {
			std::cout << name[i];
		}
		std::cout << std::endl;
		//std::copy(this->name[0], this->name[len - 1], std::ostream_iterator<char>(std::cout));
	}
};


int test10() {


	std::vector<MovingMonster> v;
	v.resize(1000000);//임의의 100만개 생성.

	std::sort(v.begin(), v.end(), [](const MovingMonster& a, const  MovingMonster& b) {
		return a.length() > b.length();
		});

	//정렬 끝.
	auto target = MovingMonster();
	std::cout << "targets len " << target.length() << std::endl;
	//move test start.
//이퀄레인지는 웬만하면 객체안에 operator를 정의해서 사용을 하는 것을 권한다. 실수의 여지가 있다. 아니라면 정렬된 상태와 같은 람다를 쓴다.


	//범위 찾기 끝. 이동 연산의 실험을 한다. 100만개가 100/8만개 만큼 균등하게 나눠져있다.
	//이범위를 나눌 때 move와 copy와 partition_copy와 초기화자리스트 어느게 효율적인 지 테스트를 한다.

	//initList;

	//first move.
	//////////////////////////////////////////////////////////////////////
	//{
	//	auto prev = std::chrono::system_clock::now();

	//	auto r = std::equal_range(v.begin(), v.end(), target, [](const MovingMoster& a, const  MovingMoster& b) {
	//		return a.length() > b.length();
	//		});

	//	std::vector<MovingMoster> v2;//reserve는 기본 베이스로
	//	v2.reserve(std::distance(r.first, r.second));
	//	std::move(r.first, r.second, std::back_inserter(v2));
	//	v.erase(r.first, r.second);
	//	auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - prev).count();
	//	std::cout <<"move erase  "<< now << "ms" << std::endl;
	//	//std::cout << v.size() << std::endl;
	//
	//	auto iter = v2.rbegin();
	//	for (int i = 0; i < 10; ++i) {
	//		(iter++)->show();
	//	}
	//	
	//	v.insert(v.end(), v2.begin(), v2.end());
	//	std::sort(v.begin(), v.end(), [](const MovingMoster& a, const  MovingMoster& b) {
	//		return a.length() > b.length();
	//		});

	//	//std::merge(v.begin(),v.end(), v2.begin(), v2.end() , std::back_inserter(v));
	//}2512
	////////////////////////////////////////////////////////////////////
	{
		auto prev = std::chrono::system_clock::now();
		std::vector<MovingMonster> v2;//reserve는 기본 베이스로
		auto r = std::partition_copy(v.begin(), v.end(), std::back_inserter(v2), v.begin(), [&target](MovingMonster& a) {
			return target.length() == a.length();
			});
		v.erase(r.second, v.end());

		auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - prev).count();
		std::cout << "partitioning  " << now << "ms" << std::endl;
		auto iter = v2.rbegin();




		for (int i = 0; i < 10; ++i) {
			(iter++)->show();
		}

		v.insert(v.end(), v2.begin(), v2.end());
		std::sort(v.begin(), v.end(), [](const MovingMonster& a, const  MovingMonster& b) {
			return a.length() > b.length();
			});
		std::cout << v.size() << std::endl;
	}

	//for (auto&& i : v)
	//	i.show();
}