#pragma once
#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include <vector>
#include <iterator>
//inc가 들어간 단어의 횟수
int inc() {


	std::ifstream in("알파벳만 남긴 엘리스.txt");

	std::vector<std::string> stringVector{ std::istream_iterator<std::string>(in), std::istream_iterator<std::string>() };
	//std::sort(stringVector.begin(), stringVector.end());

	std::vector<std::string> stringWithInc{};

	std::string targetSequence("inc");

	size_t counts = std::count_if(stringVector.begin(), stringVector.end(), [&targetSequence, &stringWithInc](const std::string& a) {
		auto iter = std::search(a.begin(), a.end(), targetSequence.begin(), targetSequence.end()); // 범위 지정이 필수다.
		if (iter != a.end())	stringWithInc.push_back(a);

		return iter != a.end();
		});

	for (auto&& i : stringWithInc)
		std::cout << i << std::endl;

	std::cout << counts << std::endl;

	//std::cout << *iter << std::endl;
}