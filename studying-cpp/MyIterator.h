#ifndef  MY_ITERATOR_1
#define MY_ITERATOR_1

#include <iostream>
#include <iterator>
#include <initializer_list>
#include <algorithm>

//Iterator's condition for using in STL Container and Alogorithms

// all category require 1. Constructor
//						2. Unary Increase Opeartor. operator++, operator++(int);
// forward iterator		3. operator==(), operator!=()
//						4. rvaluse reference operator *a, a->m
//						5. default construct Iterator() 
// bidirectional		6. Unary Decrease Operator. operator--, operator--(int);
// random access	    7. + - Binary Mathametic Operator. operator+() , operator-()
//						8. operator < > <= >=()
//						9. operator+=(), operator-=()
//						10. index. operator[]();


template<class X>
class MyIterator
	//before cpp 17 :public std::iterator<std::random_access_iterator_tag,  X> // 이터레이터 태그 이후로는 value_type과 reference pointer difference_type에 대하여 정의한다.
	// 없을경우는 void로 채워서 사용했었다.
{
public:
	// in cpp 17
	using value_type			= X;
	using reference				= X &;
	using pointer				= X *;
	using iterator_category		= std::random_access_iterator_tag;
	using difference_type		= std::ptrdiff_t;

	/*
		typedef X value_type;
		typedef X& reference;
		typedef X* pointer;
		typedef std::ptrdiff_t difference_type;
		typedef std::random_access_iterator_tag iterator_category;
	*/
private:
	pointer elem;
public:
	MyIterator(pointer elem = nullptr)	noexcept : elem{ elem } //default ,constructor
	{
		std::cout << "생성자 호출" << std::endl;
	}
	~MyIterator()noexcept //default ,constructor
	{
		std::cout << "소멸자 호출" << std::endl;
	}
	MyIterator(const MyIterator& other)	noexcept : elem{ other.elem } //default ,constructor
	{
		std::cout << "복사 생성자 호출" << std::endl;
	}
	MyIterator(const MyIterator&& xValOther)	noexcept : elem{ std::move(xValOther.elem) } //default ,constructor
	{
		std::cout << "이동 생성자 호출" << std::endl;
	}

	MyIterator& operator++(void) {
		std::cout << "전위 증가" << std::endl;
		++this->elem;
		return *this;
	}


	MyIterator operator++(int) {
		std::cout << "후위 증가" << std::endl;
		MyIterator nrvo{ *this };
		++this->elem;
		return nrvo;
	}

	reference operator*(void) const {
		return *this->elem;
	}

	pointer operator->(void) const { //l val 
		return this->elem;
	}

	//forward iterator 가 갖춰야 할 연산목록 <여기까지 갖고 있을 시에 vector, deque에서 돌아가는 이터레이터가 된다.>


	MyIterator& operator--(void) {
		std::cout << "전위 감소" << std::endl;
		--this->elem;
		return *this;
	}

	MyIterator operator--(int) {
		std::cout << "후위 감소" << std::endl;
		MyIterator nrvo{ *this };
		--this->elem;
		return nrvo;
	}
	//bidirectional iterator


	reference operator[](difference_type idx) {
		return this->elem[idx];
	}

	MyIterator operator+(difference_type rhs)const {
		return MyIterator(this->elem + rhs);
	}

	MyIterator operator-(difference_type rhs) const {
		return MyIterator(this->elem - rhs);
	}

	difference_type operator+(const MyIterator& rhs)const {
		return this->elem + rhs.elem;
	}

	difference_type operator-(const MyIterator& rhs) const {
		return this->elem - rhs.elem;
	}

	//operator + global version
	friend MyIterator operator+(difference_type lhs, const MyIterator& rhs) {
		return MyIterator(lhs + rhs.elem);
	}
	friend MyIterator operator-(difference_type lhs, const MyIterator& rhs) {
		return MyIterator(lhs - rhs.elem);
	}

	MyIterator& operator=(pointer rhs) {
		this->elem = rhs;
		return *this;
	}

	MyIterator& operator=(const MyIterator& rhs) {
		this->elem = rhs.elem;
		return *this;
	}
	// iterator's X- Value operator=(const MyIterator&& rhs) version is useless. it is just moving  pointer.

	MyIterator& operator+=(difference_type rhs) {
		this->elem += rhs;
		return *this;
	}

	MyIterator& operator-=(difference_type rhs) {
		this->elem -= rhs;
		return *this;
	}

	// for searching iterator's displacement.
	bool operator==(const MyIterator& rhs) const {
		return this->elem == rhs.elem;
	}

	bool operator!=(const MyIterator& rhs) const {
		return (this->elem != rhs.elem);
	}

	bool operator<(const MyIterator& rhs) const {
		return this->elem < rhs.elem;
	}
	bool operator>(const MyIterator& rhs) const {
		return this->elem > rhs.elem;
	}
	bool operator<=(const MyIterator& rhs) const {
		return this->elem <= rhs.elem;
	}
	bool operator>=(const MyIterator& rhs) const {
		return this->elem >= rhs.elem;
	}

	//random access
};

template<class x, unsigned int len>
class TestDataStructure {
private:
	x data[len]{};
public:
	TestDataStructure() {

		x temp{};
		for (auto& elem : data) {
			elem = temp++;
		}
	};


	constexpr size_t size() const noexcept { // get length of list
		return static_cast<size_t>(&data[len] - data[0]);
	}

	MyIterator<x> begin(void) {
		return MyIterator<x>(&this->data[0]);
	}

	MyIterator<x> end(void) {
		return MyIterator<x>(this->data + len);
	}

	x& operator[](unsigned int idx) {
		return this->data[idx];
	}

	const x& operator[](unsigned int idx) const {
		return this->data[idx];
	}
};



#include "MyIterator.hpp"
#endif // ! MY_ITERATOR_1

