#pragma once
#include <type_traits>
#include <iterator>

template<class InputIterator>
typename InputIterator::difference_type my_distance(InputIterator begin, InputIterator end);

template<class InputIterator>
typename InputIterator::difference_type my_distance(InputIterator begin, InputIterator end) {

	if constexpr (std::is_same<std::random_access_iterator_tag, typename InputIterator::iterator_category>::value) {
		return end - begin; //when condition is false this expression is discarded;
	}
	else {//when condition is true this expression is discarded;
		typename InputIterator::difference_type counts{ };

		for (; begin != end; ++begin)
			++counts;

		return counts;
	}
}