#pragma once
#include <fstream>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <string>
#include <deque>
#include <vector>
#include <string>
#include <iomanip>
#include <list>


struct StringPair :public std::pair<std::string, std::string> {

	StringPair(std::string f) :
		pair{ f,f }
	{
		std::sort(first.begin(), first.end());
	}

	bool operator<(const StringPair& other) const noexcept {
		return this->first < other.first;
	}
};

namespace std {
	std::istream& operator>>(std::istream& is, std::pair<std::string, std::string>& out);
	std::ostream& operator<<(std::ostream& os, const std::pair<std::string, std::string>& rhs);
	std::ostream& operator<<(std::ostream& os, const StringPair& rhs);
};

void readData(std::vector<StringPair>& cont);

int test15()
{

	std::vector<StringPair> v;

	readData(v);

	std::sort(v.begin(), v.end());

	//std::ifstream in{ "애너그램들.txt" };//이렇게 읽으면 2개 페어로 고대로 집어넣은 istream을 정의한다.
	using Elem = std::pair<std::string, std::vector<std::string>>;
	std::vector< Elem > anagrams{};

	auto cur = v.begin();

	while (cur != v.end()) {

		auto anaBegin = std::adjacent_find(cur, v.end(), [](const StringPair& a, const StringPair& b) {
			return a.first == b.first;
			});

		cur = anaBegin;

		auto anaEnd = std::find_if_not(cur, v.end(), [&cur](const StringPair& a) {
			return a.first == cur->first;
			});

		if (std::distance(anaBegin, anaEnd) == 0) break;

		std::vector<std::string> temp{  };
		temp.reserve(std::distance(anaBegin, anaEnd));
		std::for_each(anaBegin, anaEnd, [&temp](const StringPair& a) {
			temp.push_back(a.second); // 원래의 값을 푸시백. 원본 수정이 가능한 경우는 무브로
			});

		anagrams.push_back(std::make_pair(anaBegin->first, std::move(temp)));
		// 벡터를 무브로 넣어주고, first 값으로 어떤 키의 아나그람인지 구분이 가능.

		cur = anaEnd;
	}

	//아나그람 내림차순 정렬

	std::sort(anagrams.begin(), anagrams.end(), [](const Elem& a, const Elem& b) {
		return a.second.size() > b.second.size();
		});

	std::ofstream out2{ "아나그람 내림차순1.txt" };
	for (const Elem& elem : anagrams) {
		out2 << "[ " << elem.second.size() << " ]	" << elem.first << "		";

		for (const std::string& e : elem.second) {
			out2 << e << "	";
		}
		out2 << "\n";
	}

	for (const Elem& elem : anagrams) {
		std::cout << "[ " << elem.second.size() << " ]	" << elem.first << "		";

		for (const std::string& e : elem.second) {
			std::cout << e << "	";
		}
		std::cout << "\n";
	}
}

void readData(std::vector<StringPair>& cont) {

	std::fstream in{ "단어들.txt" };
	for (auto i = std::istream_iterator<std::string>(in); i != std::istream_iterator<std::string>(); ++i) {
		cont.push_back(*i);
	}

	std::cout << "읽은 단어 : " << cont.size() << std::endl;

}
std::istream& std::operator>>(std::istream& is, std::pair<std::string, std::string>& out) {

	//is >> out.second;

	//out.first = out.second;
	//std::sort(out.first.begin(), out.first.end());
	return is >> out.first >> out.second;;
}

std::ostream& std::operator<<(std::ostream& os, const std::pair<std::string, std::string>& rhs) {
	os.setf(std::ios::left);
	os << std::setw(40) << rhs.first << '\t' << rhs.second << '\n';
	return os;
}

std::ostream& std::operator<<(std::ostream& os, const StringPair& rhs) {
	os.setf(std::ios::left);
	os << std::setw(40) << rhs.first << '\t' << rhs.second << '\n';
	return os;
}