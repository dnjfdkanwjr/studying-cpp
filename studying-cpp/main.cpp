#pragma once
#include <algorithm>
#include <random>
#include <iostream>
#include <iterator>
#include <chrono>
#include <set>
std::default_random_engine dre;

std::uniform_int_distribution<int> nameRandom{ 'a','z' };
std::uniform_int_distribution<int> lenRandom{ 3,10 };

// move test.
// versus copy


class MovingMonster {
	char* name{ nullptr };

	int len{ };
public:
	MovingMonster()noexcept {
		this->len = lenRandom(dre);
		this->name = new char[len];
		for (int i = 0; i < len; ++i)
			this->name[i] = nameRandom(dre);
	}

	MovingMonster(const MovingMonster& other)noexcept {
		this->len = other.len;
		this->name = new char[len];

		for (int i = 0; i < len; ++i) {
			this->name[i] = other.name[i];
		}
	}


	MovingMonster(MovingMonster&& xVal)noexcept {
		this->name = std::move(xVal.name);
		xVal.name = nullptr;
		this->len = xVal.len;
	}
	~MovingMonster()  noexcept {
		delete this->name;
	}

	MovingMonster& operator=(const MovingMonster& other) noexcept {
		this->len = other.len;

		if (this->name != nullptr) delete[] this->name;
		this->name = new char[len];

		for (int i = 0; i < len; ++i) {
			this->name[i] = other.name[i];
		}

		return*this;
	}

	MovingMonster& operator=(MovingMonster&& other) noexcept {
		this->len = other.len;
		if (this->name != nullptr) delete[] this->name;
		this->name = std::move(other.name);
		other.name = nullptr;

		return*this;
	}

	int length() const noexcept {
		return this->len;
	}

	void show(void) const noexcept {
		for (int i = 0; i < len; ++i) {
			std::cout << name[i];
		}
		std::cout <<" len" <<this->len <<std::endl;
		//std::copy(this->name[0], this->name[len - 1], std::ostream_iterator<char>(std::cout));
	}

	//for remove copy && unique in list
	bool operator==(const MovingMonster& other)const noexcept {
		return this->len == other.len;
	}


};


template <typename FIterator, typename OIterator, typename Pred>
FIterator splice_if(FIterator first, FIterator last, OIterator out, Pred p)
{
	FIterator result = first;
	for (; first != last; ++first) {
		if (p(*first)) {
			*out++ = *first;
		}
		else {
			*result++ = *first;
		}
	}
	return result;
}


struct MyLess {//set에서 사용하는 객체는 펑터다.

	bool operator()(const MovingMonster& a, const  MovingMonster& b) {
		return a.length() < b.length();
	}
};

#include <numeric>
int main() {

	//std::multiset<MovingMonster, MyLess> sets;// set을 찾으려면 operator< 정의되어있어야함 키값은 모두 레스로 찾는다. 맵도 마찬가지


	//std::vector<MovingMonster> v;

	//단 하나 등수를 찾을땐 무조건 nth_element다.
	

	std::vector<int> v;
	v.resize(300);
	for (int i = 0; i < 100; ++i) {
		v[i] = 100;
	}
	v[99] = 101;
	for (int i = 100; i < 300; ++i) {
		v[i] = 200;
	}
	std::random_shuffle(v.begin(), v.end());
	{

		auto prev = std::chrono::system_clock::now();
		std::nth_element(v.begin(), v.begin() + 99, v.end()); // +99을하면 99번째 idx를 찾는다. v[100]이전까지  . 0~99 ( 즉 99의 idx 100번째 원소를 찾는 것.)
		 // nth_element 를 이용하여 N번째로 작은 원소를
		 // v의 N번째 위치인, v.begin() + N 에 놓는다. 순서상으론 100번째 원소가 된다. +0하면 첫번째 원소를 찾는것.

		auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - prev).count();
		std::cout << "nth = " << now << "ms" << std::endl;


	}
	std::cout << *(v.begin() + 99) <<" idx 100번째" << v[99] <<  std::endl;

	//for (int i = 0; i < 300; ++i) {
	//	std::cout << " i = " << i + 1 << "  ";

	//	std::cout << v[i] << std::endl;

	//}

	std::random_shuffle(v.begin(), v.end());

	std::random_shuffle(v.begin(), v.end());
	{	
		auto prev = std::chrono::system_clock::now();
		std::partial_sort(v.begin(), v.begin() + 100, v.end());
		// begin() + N에서 N은 포함하지않고 N-1까지 소트 정확히 원소 N개를 소트하는 것임. idx로 치면 N-1
		auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - prev).count();
		std::cout << "partial_sort = " << now << "ms" << std::endl;
	}
	//for (int i = 0; i < 300; ++i) {
	//	std::cout << " i = " << i+1 << "  ";

	//	std::cout << v[i] << std::endl;

	//}



}


int test44() {


	//std::vector<MovingMonster> v;
	//v.resize(1000000);//임의의 100만개 생성.

	//std::sort(v.begin(), v.end(), [](const MovingMonster& a, const  MovingMonster& b) {
	//	return a.length() < b.length();
	//	});

	////정렬 끝.
	//auto target = MovingMonster();
	//std::cout << "targets len " << target.length() << std::endl;
	//move test start.
//이퀄레인지는 웬만하면 객체안에 operator를 정의해서 사용을 하는 것을 권한다. 실수의 여지가 있다. 아니라면 정렬된 상태와 같은 람다를 쓴다.


	//범위 찾기 끝. 이동 연산의 실험을 한다. 100만개가 100/8만개 만큼 균등하게 나눠져있다.
	//이범위를 나눌 때 move와 copy와 partition_copy와 초기화자리스트 어느게 효율적인 지 테스트를 한다.

	//initList;

	//first move.
	//////////////////////////////////////////////////////////////////////
	//{
	//	auto prev = std::chrono::system_clock::now();

	//	auto r = std::equal_range(v.begin(), v.end(), target, [](const MovingMoster& a, const  MovingMoster& b) {
	//		return a.length() > b.length();
	//		});

	//	std::vector<MovingMoster> v2;//reserve는 기본 베이스로
	//	v2.reserve(std::distance(r.first, r.second));
	//	std::move(r.first, r.second, std::back_inserter(v2));
	//	v.erase(r.first, r.second);
	//	auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - prev).count();
	//	std::cout <<"move erase  "<< now << "ms" << std::endl;//2512ms
	//	//std::cout << v.size() << std::endl;
	//
	//	auto iter = v2.rbegin();
	//	for (int i = 0; i < 10; ++i) {
	//		(iter++)->show();
	//	}
	//	
	//	v.insert(v.end(), v2.begin(), v2.end());
	//	std::sort(v.begin(), v.end(), [](const MovingMoster& a, const  MovingMoster& b) {
	//		return a.length() > b.length();
	//		});

	//	//std::merge(v.begin(),v.end(), v2.begin(), v2.end() , std::back_inserter(v));
	//}

	////////////////////////////////////////////////////////////////////
	//{
	//	auto prev = std::chrono::system_clock::now();
	//	std::vector<MovingMoster> v2;//reserve는 기본 베이스로
	//	auto r = std::partition_copy(v.begin(), v.end(), std::back_inserter(v2), v.begin(), [&target](MovingMoster& a) {
	//		return target.length() == a.length();
	//		});
	//	v.erase(r.second, v.end());

	//	auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - prev).count();
	//	std::cout << "partitioning  " << now << "ms" << std::endl;
	//	auto iter = v2.rbegin();




	//	for (int i = 0; i < 10; ++i) {
	//		(iter++)->show();
	//	}

	//	v.insert(v.end(), v2.begin(), v2.end());
	//	std::sort(v.begin(), v.end(), [](const MovingMoster& a, const  MovingMoster& b) {
	//		return a.length() > b.length();
	//		});
	//	std::cout << v.size() << std::endl;
	//}
	//{
	//	std::cout << target.length() << std::endl;
	//	auto prev = std::chrono::system_clock::now();
	//	std::vector<MovingMonster> v2;//reserve는 기본 베이스로
	//	
	//	//(std::remove_copy_if(v.begin(), v.end(), std::back_inserter(v2), [&target](MovingMonster& i) {
	//	//	return i.length() == target.length();
	//	//	}),v.end()); 

	//	//단순 리무브 커피 오름차순일때만 동작한다.

	//	//v.erase(splice_if(v.begin(), v.end(), std::back_inserter(v2), [&target](MovingMonster& i) {
	//	//	return i.length() == target.length();
	//	//	}), v.end());
	//	//v.erase(,v.end());

	//	auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - prev).count();
	//	std::cout << "remove_copy  " << now << "ms" << std::endl;//69000~80000ms
	//	std::cout << v.size() <<" 원본의 사이즈" << std::endl;
	//	std::cout << v2.size() << " V2의 사이즈" << std::endl;
	//	auto iter = v2.rbegin();

	//	for (int i = 0; i < 10; ++i) {
	//		(iter++)->show();
	//	}
	//}
	//for (auto&& i : v)
	//	i.show();
}