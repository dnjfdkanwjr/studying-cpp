#include <iostream>
#include <iterator>
#include <algorithm>
#include <fstream>
#include <vector>
#include <string>


#include <DirectXMath.h>


using namespace DirectX;

void show(XMVECTOR tempPos) {


	XMFLOAT4 calcPos;

	XMStoreFloat4(&calcPos, tempPos);

	std::cout << calcPos.x << " " << calcPos.y << " " << calcPos.z << std::endl;
}

int transformingCoordinateSys() {

	XMVECTOR posA = XMVectorSet(2500, 0, 2500, 1); //when w value is 1, this means this is point. else this is vector.
	XMVECTOR tp = XMVectorSet(2500, 0, 2500, 1);


	XMVECTOR posWorld = XMVectorSet(0, 0, 0, 1);

	float radX = XMConvertToRadians(0);
	float radY = XMConvertToRadians(180);
	float radZ = XMConvertToRadians(0);
	XMVECTOR vRotA = DirectX::XMQuaternionRotationRollPitchYaw(radX, radY, radZ);

	auto posATMat = DirectX::XMMatrixTranslationFromVector(posA);
	auto rotMatA = XMMatrixRotationQuaternion(vRotA);
	auto transformMatA = rotMatA * posATMat;

	auto invTransformA = XMMatrixTranspose(transformMatA); // naturally invers matrix of transforming matrix is their transpose mat;

	XMVECTOR vRotWorld = DirectX::XMQuaternionRotationRollPitchYaw(0, 0, 0);
	auto posWorldTMat = DirectX::XMMatrixTranslationFromVector(posWorld);
	auto rotMatWorld = XMMatrixRotationQuaternion(vRotWorld);
	auto transformMatWorld = rotMatWorld * posWorldTMat;


	auto tempPos = XMVector4Transform(tp, invTransformA);
	auto tempPos2 = XMVector4Transform(tp, transformMatWorld);

	std::cout << "world" << std::endl;
	show(tempPos2);

	std::cout << "local" << std::endl;
	show(tempPos);
}