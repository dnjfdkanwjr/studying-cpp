#pragma once
#include <fstream>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <map>
#include <unordered_map>
#include <set>
#include <string>
#include <deque>
#include <vector>
#include <chrono>
#include <random>

//anagram optimization parctice
namespace std {

	std::istream& operator>>(std::istream& is, std::pair<std::string, std::string>& out);
	std::ostream& operator<<(std::ostream& os, const std::pair<std::string, std::string>& rhs);
}

std::default_random_engine dre;
std::uniform_int_distribution<int> uidForStr{ 'a', 'z' };
std::uniform_int_distribution<int> uidForLength{ 1,31 };
int test()
{
	std::fstream in{ "단어들.txt" };
	std::fstream in2{ "랜덤단어.txt" };
	//1000만번 찾기 수행 테스트

	std::vector < std::deque < unsigned long long >> averageTime;
	averageTime.resize(4);
	std::vector<std::string> targetList{ std::istream_iterator<  std::string>(in2),
		std::istream_iterator< std::string>() };

	std::multimap<std::string, std::string> dict{ std::istream_iterator< std::pair<std::string, std::string>>(in),
		std::istream_iterator<std::pair<std::string, std::string>>() };


	std::vector<std::pair<std::string, std::string>> dict2{ dict.begin(),
	dict.end() };


	std::unordered_multimap<std::string, std::string> dict3{
		dict.begin(),dict.end()
	};


	unsigned long long testTime = 0;
	unsigned long long testTime2 = 0;
	unsigned long long testTime3 = 0;
	unsigned long long testTime4 = 0;

	int k = 0;
	std::string forTestInput;
	std::string input;
	int i = 0;

	while (forTestInput != "0") {

		forTestInput = std::to_string((i % 4) + 1);
		if (++i > 20)	forTestInput = "10";

		if (forTestInput == "4") {
			k = 0;
			while (k < targetList.size()) {
				//std::cout << " 아나그램 입력 :  ";
				//std::cin >> input;
				++k;
				input = targetList[k];
				std::sort(input.begin(), input.end());


				auto prev = std::chrono::system_clock::now();

				if (dict3.count(input)) {
					auto range = dict3.equal_range(input);
					/*	for (auto&& iter = range.first; iter != range.second; ++iter) {
							std::cout << *iter << std::endl;
						}*/
						//for (auto&& i = dict.lower_bound(input); i != dict.upper_bound(input); ++i) {
						//	std::cout << *i << std::endl;
						//}
				}
				else {
					//std::cout << " 아나그램 없다." << std::endl;
				}
				auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - prev).count();
				//std::cout << now << std::endl;
				testTime4 += now;
				input.clear();

			}
			averageTime[3].push_back(testTime4);
			std::cout << "언오더드 멀티맵의 수행 속도 " << testTime4 << " MS " << std::endl; testTime4 = 0;
		}
		else if (forTestInput == "3") {
			k = 0;
			while (k < targetList.size()) {
				//std::cout << " 아나그램 입력 :  ";
				//std::cin >> input;
				++k;
				input = targetList[k];
				std::sort(input.begin(), input.end());


				auto prev = std::chrono::system_clock::now();

				if (dict.count(input)) {
					auto range = dict.equal_range(input);
					/*	for (auto&& iter = range.first; iter != range.second; ++iter) {
							std::cout << *iter << std::endl;
						}*/
						//for (auto&& i = dict.lower_bound(input); i != dict.upper_bound(input); ++i) {
						//	std::cout << *i << std::endl;
						//}
				}
				else {
					//std::cout << " 아나그램 없다." << std::endl;
				}
				auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - prev).count();
				//std::cout << now << std::endl;
				testTime += now;
				input.clear();

			}
			averageTime[0].push_back(testTime);
			std::cout << "멀티맵의 수행 속도" << testTime << " MS " << std::endl; testTime = 0;
		}
		else if (forTestInput == "2") {
			k = 0;
			while (k < targetList.size()) {
				++k;
				input = targetList[k];
				std::sort(input.begin(), input.end());
				auto targetVal = std::make_pair(input, input);

				auto prev = std::chrono::system_clock::now();
				auto range = std::equal_range(std::begin(dict2), std::end(dict2), targetVal, [](auto&& a, auto&& b) {
					return a.first < b.first;
					});

				auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - prev).count();

				testTime2 += now;
				input.clear();
			}
			averageTime[1].push_back(testTime2);
			std::cout << "벡터의 수행 속도 단순 이퀄 레인지" << testTime2 << " MS " << std::endl; testTime2 = 0;
		}
		else if (forTestInput == "1") {

			k = 0;

			while (k < targetList.size()) {
				++k;
				input = targetList[k];

				auto targetVal = std::make_pair(input, input);
				auto prev = std::chrono::system_clock::now();

				if (std::binary_search(dict2.begin(), dict2.end(), targetVal, [](auto&& a, auto&& b) {
					return a.first < b.first;
					}))
				{
					auto range = std::equal_range(std::begin(dict2), std::end(dict2), targetVal, [](auto&& a, auto&& b) {
						return a.first < b.first;
						});

				}
				else {

				}
				auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - prev).count();

				testTime3 += now;
				input.clear();
			}
			averageTime[2].push_back(testTime3);
			std::cout << "벡터의 수행 속도 단순 BSearch후 이퀄레인지" << testTime3 << " MS " << std::endl;
			testTime3 = 0;

		}
		else if (forTestInput == "10") {
			std::cout << "하이" << std::endl;
			std::ofstream outResults{ "콘테이너별속도결과.txt" };
			unsigned long long	temp = 0;
			for (int i = 0; i != averageTime[0].size(); ++i) {
				temp += averageTime[0][i];
			}
			temp /= float(averageTime[0].size());
			std::cout << temp << "멀티맵 평균" << std::endl;
			outResults << " 멀티맵 테스트 횟수 (1000만)  " << averageTime[0].size() << std::endl;
			outResults << " 소요 평균 시간 (걸린시간/횟수)  " << temp << std::endl;

			outResults << " 벡터 이퀄레인지만 테스트 횟수(1000만)  " << averageTime[1].size() << std::endl;
			temp = 0;
			for (int i = 0; i != averageTime[1].size(); ++i) {
				temp += averageTime[1][i];
			}
			temp /= float(averageTime[1].size());
			std::cout << temp << "이퀄레인지만 평균" << std::endl;
			outResults << " 소요 평균 시간 (걸린시간/횟수)  " << temp << std::endl;

			outResults << " 벡터 이진탐색,이퀄레인지 테스트 횟수(1000만)  " << averageTime[2].size() << std::endl;
			temp = 0;
			for (int i = 0; i != averageTime[2].size(); ++i) {
				temp += averageTime[2][i];
			}
			temp /= float(averageTime[2].size());
			std::cout << temp << "이진 이퀄레인지  평균" << std::endl;
			outResults << " 소요 평균 시간 (걸린시간/횟수)  " << temp << std::endl;

			outResults << " 언오더드 멀티맵 탐색 (1000만)  " << averageTime[2].size() << std::endl;
			temp = 0;
			for (int i = 0; i != averageTime[3].size(); ++i) {
				temp += averageTime[3][i];
			}
			temp /= float(averageTime[3].size());
			std::cout << temp << "언오더드 맵 평균" << std::endl;
			outResults << " 소요 평균 시간 (걸린시간/횟수)  " << temp << std::endl;




			forTestInput = "0";
			break;
		}


		forTestInput.clear();

	}

}


std::istream& std::operator>>(std::istream& is, std::pair<std::string, std::string>& out) {
	is >> out.second;
	out.first = out.second;
	std::sort(out.first.begin(), out.first.end());
	return is;
}

std::ostream& std::operator<<(std::ostream& os, const std::pair<std::string, std::string>& rhs) {
	os << rhs.first << "   " << rhs.second;
	return os;
}

