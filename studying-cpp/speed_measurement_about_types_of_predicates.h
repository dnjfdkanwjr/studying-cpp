#include <iostream>
#include <chrono>
#include <map>
#include <unordered_map>
#include <random>
#include <numeric>

//표본이 적을때는 성능상 비슷해 보일 수 있다. 함수포인터, 함수객체, 람다
//표본이 많을 때는 성능의 차이가 두드러진다. 람다 == 함수객체 > 함수포인터 순이다.

class FO {


public:
	bool operator()(int  a, int b) {
		return a < b;
	}
};

bool F(int a, int b) {
	return a < b;
}
int test3()
{
	//맵의 찾기 방법
	//std::map <int, int > m;

	////m에 정수 1000만개를 넣는다.
	//std::uniform_int_distribution<int> uid;
	//std::default_random_engine dre;

	//int i = 0;
	//while (m.size() != 10000000)
	//	m.emplace(uid(dre), i++);


	//std::cout << " m의 사이즈 " << m.size() << std::endl;
	//std::cout << i - m.size() << std::endl;

	////언오더드 맵에 같은 정수 천만개를 넣는다.

	//std::unordered_map<int, int> um{ m.cbegin() , m.cend(), m.size() };

	//std::cout << " um의 사이즈  " << um.size() << std::endl;

	//std::cout << " um의 버킷사이즈  " << um.bucket_count() << std::endl;

	//const int targetVal = uid(dre);

	//std::cout << " 벡터에 임의 정수 100만개를 담는다." << std::endl;

	//std::vector<unsigned int> v{};

	//v.reserve(1000000);
	//for (int i = 0; i < 1000000; ++i)
	//	v.emplace_back(uid(dre));

	////임의의 키가 있는 지 찾아본다.
	//{
	//	std::cout << " 찾는 시간 비교  맵 " << std::endl;

	//	//임의의 원소 백만개를 찾아서 시간을 테스트한다.

	//	auto prevTime = std::chrono::system_clock::now();

	//	int i = 0;

	//	for (i = 0; i < 1000000; ++i)
	//		m[v[i]];

	//	auto curTime = std::chrono::system_clock::now() - prevTime;
	//	std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(curTime).count() << " 밀리 초" << std::endl;



	//}

	//{
	//	std::cout << " 찾는 시간 비교  언오더드 맵 " << std::endl;

	//	//임의의 원소 백만개를 찾아서 시간을 테스트한다.

	//	auto prevTime = std::chrono::system_clock::now();

	//	int i = 0;

	//	for (i = 0; i < 1000000; ++i)
	//		um[v[i]];

	//	auto curTime = std::chrono::system_clock::now() - prevTime;
	//	std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(curTime).count() << " 밀리 초" << std::endl;


	//}


	int elements{ 25 };
	std::vector<int> v(elements);
	std::iota(v.begin(), v.end(), 1);// 맨오른쪽의 값을 ++ 해가며 더한다.


	{
		std::random_shuffle(v.begin(), v.end()); // 무작위로 셔플한다. cpp 14이후로 금지

		auto prevTime = std::chrono::system_clock::now();

		std::sort(v.begin(), v.end(), [](const int& prev, const int& cur) {
			return prev > cur;
			});



		auto curTime = std::chrono::system_clock::now() - prevTime;

		std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(curTime).count() << " nano 초 (람다)" << std::endl;
	}

	{
		std::random_shuffle(v.begin(), v.end()); // 무작위로 셔플한다.
		auto prevTime = std::chrono::system_clock::now();
		std::sort(v.begin(), v.end(), FO());



		auto curTime = std::chrono::system_clock::now() - prevTime;

		std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(curTime).count() << " nano 초 (함수 객체) " << std::endl;

	}

	{
		std::random_shuffle(v.begin(), v.end()); // 무작위로 셔플한다.
		auto prevTime = std::chrono::system_clock::now();
		std::sort(v.begin(), v.end(), F);



		auto curTime = std::chrono::system_clock::now() - prevTime;

		std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(curTime).count() << " nano 초 (함수 포인터) " << std::endl;

	}



}
